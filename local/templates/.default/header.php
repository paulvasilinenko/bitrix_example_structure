<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

// Подключение css
$asset->addCss(SITE_TEMPLATE_PATH.'/markup/dist/css/all1.css');
$asset->addCss(SITE_TEMPLATE_PATH.'/markup/dist/css/all2.css');
$asset->addCss(SITE_TEMPLATE_PATH.'/markup/dist/css/all3.css');

// Подключение js
$asset->addJs(SITE_TEMPLATE_PATH.'/markup/dist/js/all1.js');
$asset->addJs(SITE_TEMPLATE_PATH.'/markup/dist/js/all2.js');
$asset->addJs(SITE_TEMPLATE_PATH.'/markup/dist/js/all3.js');

// Если страница Главная
$isMain = $APPLICATION->GetCurPage() === "/";

?><!doctype html>
<html lang="<?= LANGUAGE_ID; ?>">
<head>
    <? $APPLICATION->ShowHead(); ?>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><? $APPLICATION->ShowTitle(); ?></title>
</head>
<body>
    <div class="content <? $APPLICATION->ShowProperty("color", "white"); ?>">
        <? if(!$isMain): ?>
            <h1>
                <? $APPLICATION->ShowTitle(false); ?>
            </h1>
        <? endif; ?>