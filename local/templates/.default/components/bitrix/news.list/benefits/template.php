<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// Здесь определяем необходимые переменные,
// дополнительные расчеты
$foo = "bar";
$price = 1500;

?>

<? if (!empty($arResult["ITEMS"])): ?>

            <? foreach($arResult["ITEMS"] as $k => $arItem): ?>

                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>

                <? if ($arItem["DISPLAY_PROPERTIES"]["TEXT_POSITION"]["VALUE_XML_ID"] == "RIGTH"): ?>

                    <div class="row row--small strategy-first" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="col-6 col-md-12">
                            <div class="strategy-logo" style="background-image: url('<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE"]["FILE_VALUE"]["SRC"]; ?>')"></div>
                        </div>
                        <div class="col-6 col-md-12">
                            <div class="strategy-text">
                                <div class="strategy-text__title">
                                    <?= $arItem["~NAME"]; ?>
                                </div>
                                <div class="strategy-text__desc">
                                    <?= $arItem["PREVIEW_TEXT"]; ?>
                                </div>
                                <a href="<?= $arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]; ?>" class="strategy-text__link">
                                    <?= $arItem["DISPLAY_PROPERTIES"]["LINK_TEXT"]["VALUE"]; ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <? else: ?>

                    <div class="row row--small strategy-second" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="col-6 col-md-12">
                            <div class="strategy-text">
                                <div class="strategy-text__title">
                                    <?= $arItem["~NAME"]; ?>
                                </div>
                                <div class="strategy-text__desc">
                                    <?= $arItem["PREVIEW_TEXT"]; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <? endif; ?>

                <? if ((count($arResult["ITEMS"])-1) != $k): ?>

                    <div class="strategy-hr"></div>

                <? endif; ?>

            <? endforeach; ?>

<? endif; ?>