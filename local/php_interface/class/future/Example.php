<?php

namespace Future;

/**
 * Class Example
 * @package Future
 */
class Example
{
    /**
     * @param string $name
     * @return bool|string
     */
    public function hello($name)
    {
        if(strlen($name) > 0){

            return 'Hello, '.$name.'!<br>';

        }

        return false;
    }
}