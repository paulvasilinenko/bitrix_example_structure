<?php

require_once 'include/constants.php';
require_once 'include/functions.php';
require_once 'include/events.php';
require_once VENDOR_PATH.'/autoload.php';

/*
 * Пример использования
 * своих классов с помощью
 * автозагрузки PSR-4
 */
use Future\Example;

$example = new Example();
$example->hello('world');

printr($example);