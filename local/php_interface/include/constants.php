<?php

define('PHP_INTERFACE_PATH', dirname(dirname(__FILE__)));
define('LOCAL_PATH', dirname(PHP_INTERFACE_PATH));
define('VENDOR_PATH', LOCAL_PATH.DIRECTORY_SEPARATOR.'vendor');
define('ROOT_PATH', dirname(LOCAL_PATH));
define('BITRIX_PATH', ROOT_PATH.DIRECTORY_SEPARATOR.'bitrix');
define('BITRIX_PHP_INTERFACE_PATH', BITRIX_PATH.DIRECTORY_SEPARATOR.'php_interface');